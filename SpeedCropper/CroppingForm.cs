﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpeedCropper
{
    public partial class CroppingForm : Form
    {
        private string fileName;

        private int cropPercentage = 50;
        private double imageRatio = 0;

        private bool dragging = false;
        private int dragSourceX = 0;
        private int dragSourceY = 0;

        private double cropCenterX = 50;
        private double cropCenterY = 50;

        public CroppingForm()
        {
            InitializeComponent();

            imageBox.MouseWheel += imageBox_MouseWheel;
            imageBox.Paint += drawCroppingArea;
        }

        private void CroppingForm_Load(object sender, EventArgs e)
        {
            outputDirTextBox.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
        }

        private void loadImage(String fullFileName)
        {
            if (!File.Exists(fullFileName))
            {
                MessageBox.Show("The chosen file does not exist:\n"+fullFileName, "File does not exist!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
                
            fileName = Path.GetFileName(fullFileName);
            
            cropPercentage = 50;

            imageBox.Image = Image.FromFile(fullFileName);
            imageRatio = imageBox.Image.Width * 1.0 / imageBox.Image.Height;

            this.Text = String.Format("{2} - {0} x {1} - Speed Cropper", imageBox.Image.Width, imageBox.Image.Height, fileName);

            
        }

        // Adapted from Solution 2 of http://www.codeproject.com/Questions/549746/drawplusrectangleplusonpluspictureboxplusimage
        void drawCroppingArea(object sender, PaintEventArgs e)
        {
            int width = e.ClipRectangle.Width;
            int height = e.ClipRectangle.Height;
            int centerX = (int)(width * cropCenterX / 100);
            int centerY = (int)(height * cropCenterY / 100);
            double clipRatio = width * 1.0 / height;
            

            int w, h;

            if(clipRatio > imageRatio) // clip is wider, use height
            {
                h = (int)(height / 100.0 * cropPercentage);
                w = (int)(h*widthChooser.Value/heightChooser.Value); // Change this to use aspect ratio
            }
            else // using width else does not seem to work
            {
                w = (int)(width / 100.0 * cropPercentage);
                h = w; // Change this to use aspect ratio
            }


            e.Graphics.DrawRectangle(new Pen(Color.Red, 2f), new Rectangle(centerX - w/2, centerY - h/2, w, h));
        }

        private void imageBox_MouseWheel(object sender, MouseEventArgs e)
        {
            int factor = 1;
            if (e.Y < imageBox.Height / 2) // Upper half
                factor = 5;
            cropPercentage += factor * Math.Sign(e.Delta); // We might want to use the real value of Delta, but it is often large
            if (cropPercentage > 100)
                cropPercentage = 100;
            else if (cropPercentage < 0)
                cropPercentage = 0;

            percentageCropChooser.Text = String.Format("{0} %", cropPercentage);

            imageBox.Invalidate();
        }

        private void imageBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                cropCenterX += (e.X - dragSourceX) * 1.0 / imageBox.Width * 100;
                cropCenterY += (e.Y - dragSourceY) * 1.0 / imageBox.Height * 100;

                imageBox.Invalidate();

                dragSourceX = e.X;
                dragSourceY = e.Y;
            }
        }

        private void imageBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (!dragging) {
                if (e.Button == MouseButtons.Left) {
                    dragSourceX = e.X;
                    dragSourceY = e.Y;
                    dragging = true;
                }
            }
        }

        private void imageBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (dragging) { 
                dragging = false;
            }
        }

        // Adapted from http://stackoverflow.com/questions/17520976/crop-an-image-in-imagebox-control-and-save-it
        private void saveButton_Click(object sender, EventArgs e)
        {
            if (fileName == null)
            {
                MessageBox.Show("Open a file to crop.", "No file loaded!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return; 
            }

            if (!Directory.Exists(outputDirTextBox.Text))
            {
                MessageBox.Show("Output directory does not exist.", "File does not exist!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
                
            string fullFileName = Path.Combine(outputDirTextBox.Text, fileName);
            if (File.Exists(fullFileName))
            {
                MessageBox.Show("The output file already exists:n"+fullFileName, "File exists!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }



            int targetWidth = (int)widthChooser.Value;
            int targetHeight = (int)heightChooser.Value;

            int sourceWidth = imageBox.Image.Width;
            int sourceHeight = imageBox.Image.Height;

            int centerX = (int)(sourceWidth * cropCenterX / 100);
            int centerY = (int)(sourceHeight * cropCenterY / 100);
            int cropHeight = (int)(sourceHeight / 100.0 * cropPercentage);
            int cropWidth = (int)(cropHeight * widthChooser.Value / heightChooser.Value);

            Bitmap target = new Bitmap(targetWidth, targetHeight);
            target.SetResolution(imageBox.Image.HorizontalResolution, imageBox.Image.VerticalResolution);

            Graphics g = Graphics.FromImage(target);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            g.DrawImage(imageBox.Image, new Rectangle(0, 0, targetWidth, targetHeight),
                        new Rectangle(centerX - cropWidth/2, centerY - cropHeight/2, cropWidth, cropHeight), GraphicsUnit.Pixel);

            target.Save(fullFileName, ImageFormat.Jpeg);
        }

        private void outputDirSelectButton_Click(object sender, EventArgs e)
        {
            outputDirDialog.ShowDialog();
            outputDirTextBox.Text = outputDirDialog.SelectedPath;
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            openSourceFileDialog.ShowDialog();
            loadImage(openSourceFileDialog.FileName);
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            loadImage("C:\\Users\\Johannes\\Pictures\\Släktträff 2012\\IMG_5068.JPG");
        }

    }
}
