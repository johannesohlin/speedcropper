﻿namespace SpeedCropper
{
    partial class CroppingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imageBox = new System.Windows.Forms.PictureBox();
            this.cropGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.percentageLabel = new System.Windows.Forms.Label();
            this.percentageCropChooser = new System.Windows.Forms.Label();
            this.widthLabel = new System.Windows.Forms.Label();
            this.heightLabel = new System.Windows.Forms.Label();
            this.widthChooser = new System.Windows.Forms.NumericUpDown();
            this.heightChooser = new System.Windows.Forms.NumericUpDown();
            this.outputGroupBox = new System.Windows.Forms.GroupBox();
            this.outputDirSelectButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.sizeLabel = new System.Windows.Forms.Label();
            this.outputDirTextBox = new System.Windows.Forms.TextBox();
            this.outputDirLabel = new System.Windows.Forms.Label();
            this.sourceGroupBox = new System.Windows.Forms.GroupBox();
            this.testButton = new System.Windows.Forms.Button();
            this.openButton = new System.Windows.Forms.Button();
            this.openSourceFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.outputDirDialog = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox)).BeginInit();
            this.cropGroupBox.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.widthChooser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightChooser)).BeginInit();
            this.outputGroupBox.SuspendLayout();
            this.sourceGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageBox
            // 
            this.imageBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.imageBox.Location = new System.Drawing.Point(12, 12);
            this.imageBox.Name = "imageBox";
            this.imageBox.Size = new System.Drawing.Size(563, 316);
            this.imageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox.TabIndex = 0;
            this.imageBox.TabStop = false;
            this.imageBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imageBox_MouseDown);
            this.imageBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageBox_MouseMove);
            this.imageBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imageBox_MouseUp);
            // 
            // cropGroupBox
            // 
            this.cropGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cropGroupBox.Controls.Add(this.tableLayoutPanel1);
            this.cropGroupBox.Location = new System.Drawing.Point(581, 99);
            this.cropGroupBox.Name = "cropGroupBox";
            this.cropGroupBox.Size = new System.Drawing.Size(197, 101);
            this.cropGroupBox.TabIndex = 1;
            this.cropGroupBox.TabStop = false;
            this.cropGroupBox.Text = "Crop";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.percentageLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.percentageCropChooser, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.widthLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.heightLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.widthChooser, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.heightChooser, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(185, 71);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // percentageLabel
            // 
            this.percentageLabel.AutoSize = true;
            this.percentageLabel.Location = new System.Drawing.Point(3, 0);
            this.percentageLabel.Name = "percentageLabel";
            this.percentageLabel.Size = new System.Drawing.Size(62, 13);
            this.percentageLabel.TabIndex = 0;
            this.percentageLabel.Text = "Percentage";
            // 
            // percentageCropChooser
            // 
            this.percentageCropChooser.AutoSize = true;
            this.percentageCropChooser.Location = new System.Drawing.Point(95, 0);
            this.percentageCropChooser.Name = "percentageCropChooser";
            this.percentageCropChooser.Size = new System.Drawing.Size(30, 13);
            this.percentageCropChooser.TabIndex = 1;
            this.percentageCropChooser.Text = "50 %";
            // 
            // widthLabel
            // 
            this.widthLabel.AutoSize = true;
            this.widthLabel.Location = new System.Drawing.Point(3, 25);
            this.widthLabel.Name = "widthLabel";
            this.widthLabel.Size = new System.Drawing.Size(35, 13);
            this.widthLabel.TabIndex = 2;
            this.widthLabel.Text = "Width";
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.Location = new System.Drawing.Point(3, 50);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(38, 13);
            this.heightLabel.TabIndex = 3;
            this.heightLabel.Text = "Height";
            // 
            // widthChooser
            // 
            this.widthChooser.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.widthChooser.Location = new System.Drawing.Point(95, 28);
            this.widthChooser.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.widthChooser.Name = "widthChooser";
            this.widthChooser.Size = new System.Drawing.Size(87, 20);
            this.widthChooser.TabIndex = 4;
            this.widthChooser.Value = new decimal(new int[] {
            1920,
            0,
            0,
            0});
            // 
            // heightChooser
            // 
            this.heightChooser.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.heightChooser.Location = new System.Drawing.Point(95, 53);
            this.heightChooser.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.heightChooser.Name = "heightChooser";
            this.heightChooser.Size = new System.Drawing.Size(87, 20);
            this.heightChooser.TabIndex = 5;
            this.heightChooser.Value = new decimal(new int[] {
            1080,
            0,
            0,
            0});
            // 
            // outputGroupBox
            // 
            this.outputGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.outputGroupBox.Controls.Add(this.outputDirSelectButton);
            this.outputGroupBox.Controls.Add(this.saveButton);
            this.outputGroupBox.Controls.Add(this.sizeLabel);
            this.outputGroupBox.Controls.Add(this.outputDirTextBox);
            this.outputGroupBox.Controls.Add(this.outputDirLabel);
            this.outputGroupBox.Location = new System.Drawing.Point(582, 217);
            this.outputGroupBox.Name = "outputGroupBox";
            this.outputGroupBox.Size = new System.Drawing.Size(200, 111);
            this.outputGroupBox.TabIndex = 2;
            this.outputGroupBox.TabStop = false;
            this.outputGroupBox.Text = "Output";
            // 
            // outputDirSelectButton
            // 
            this.outputDirSelectButton.Location = new System.Drawing.Point(162, 35);
            this.outputDirSelectButton.Name = "outputDirSelectButton";
            this.outputDirSelectButton.Size = new System.Drawing.Size(26, 23);
            this.outputDirSelectButton.TabIndex = 4;
            this.outputDirSelectButton.Text = "...";
            this.outputDirSelectButton.UseVisualStyleBackColor = true;
            this.outputDirSelectButton.Click += new System.EventHandler(this.outputDirSelectButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(14, 81);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(174, 23);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // sizeLabel
            // 
            this.sizeLabel.AutoSize = true;
            this.sizeLabel.Location = new System.Drawing.Point(14, 64);
            this.sizeLabel.Name = "sizeLabel";
            this.sizeLabel.Size = new System.Drawing.Size(30, 13);
            this.sizeLabel.TabIndex = 2;
            this.sizeLabel.Text = "Size:";
            // 
            // outputDirTextBox
            // 
            this.outputDirTextBox.Location = new System.Drawing.Point(14, 37);
            this.outputDirTextBox.Name = "outputDirTextBox";
            this.outputDirTextBox.Size = new System.Drawing.Size(143, 20);
            this.outputDirTextBox.TabIndex = 1;
            // 
            // outputDirLabel
            // 
            this.outputDirLabel.AutoSize = true;
            this.outputDirLabel.Location = new System.Drawing.Point(11, 20);
            this.outputDirLabel.Name = "outputDirLabel";
            this.outputDirLabel.Size = new System.Drawing.Size(52, 13);
            this.outputDirLabel.TabIndex = 0;
            this.outputDirLabel.Text = "Directory:";
            // 
            // sourceGroupBox
            // 
            this.sourceGroupBox.Controls.Add(this.testButton);
            this.sourceGroupBox.Controls.Add(this.openButton);
            this.sourceGroupBox.Location = new System.Drawing.Point(587, 13);
            this.sourceGroupBox.Name = "sourceGroupBox";
            this.sourceGroupBox.Size = new System.Drawing.Size(185, 80);
            this.sourceGroupBox.TabIndex = 3;
            this.sourceGroupBox.TabStop = false;
            this.sourceGroupBox.Text = "Source";
            // 
            // testButton
            // 
            this.testButton.Location = new System.Drawing.Point(107, 30);
            this.testButton.Name = "testButton";
            this.testButton.Size = new System.Drawing.Size(66, 23);
            this.testButton.TabIndex = 1;
            this.testButton.Text = "Test";
            this.testButton.UseVisualStyleBackColor = true;
            this.testButton.Click += new System.EventHandler(this.testButton_Click);
            // 
            // openButton
            // 
            this.openButton.Location = new System.Drawing.Point(26, 30);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(75, 23);
            this.openButton.TabIndex = 0;
            this.openButton.Text = "Open";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // openSourceFileDialog
            // 
            this.openSourceFileDialog.DefaultExt = "jpg";
            // 
            // CroppingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 340);
            this.Controls.Add(this.sourceGroupBox);
            this.Controls.Add(this.outputGroupBox);
            this.Controls.Add(this.cropGroupBox);
            this.Controls.Add(this.imageBox);
            this.Name = "CroppingForm";
            this.Text = "SpeedCropper";
            this.Load += new System.EventHandler(this.CroppingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox)).EndInit();
            this.cropGroupBox.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.widthChooser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightChooser)).EndInit();
            this.outputGroupBox.ResumeLayout(false);
            this.outputGroupBox.PerformLayout();
            this.sourceGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox imageBox;
        private System.Windows.Forms.GroupBox cropGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label percentageLabel;
        private System.Windows.Forms.Label percentageCropChooser;
        private System.Windows.Forms.Label widthLabel;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.GroupBox outputGroupBox;
        private System.Windows.Forms.Label sizeLabel;
        private System.Windows.Forms.TextBox outputDirTextBox;
        private System.Windows.Forms.Label outputDirLabel;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.NumericUpDown widthChooser;
        private System.Windows.Forms.NumericUpDown heightChooser;
        private System.Windows.Forms.Button outputDirSelectButton;
        private System.Windows.Forms.GroupBox sourceGroupBox;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.Button testButton;
        private System.Windows.Forms.OpenFileDialog openSourceFileDialog;
        private System.Windows.Forms.FolderBrowserDialog outputDirDialog;
    }
}

